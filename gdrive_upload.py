#!/usr/bin/env python3
# -*- coding: utf-8 -*-

DOCUMENTATION = '''
---
script: gdrive_upload.py
description:
    - Upload files of a directory local to Google Drive in a subfolder

requirements:
    - PyDrive2 >= 1.8.0
    - Client ID 
    - Client secret

    Drive API requires OAuth2.0 for authentication. PyDrive2 makes your life much easier by handling complex authentication steps for you.
    See: https://iterative.github.io/PyDrive2/docs/build/html/quickstart.html#authentication

author: "Fabio Coelho (@fabiocruzcoelho) <fabiocruzcoelho@gmail.com>"
todo:
    - Verify error Exit if the parent folder doesn't exist
    - Add arguments:
        src_folder_name
        dst_folder_name
        parent_folder_name
'''

EXAMPLES = '''
# Example from settings.yaml

client_config_backend: settings
client_config:
  client_id: <INSERT YOUR CLIENT_ID HERE>
  client_secret: <INSERT YOUR SECRET HERE>
  auth_uri: https://accounts.google.com/o/oauth2/auth
  token_uri: https://accounts.google.com/o/oauth2/token
  redirect_uri: urn:ietf:wg:oauth:2.0:oob
  revoke_uri:

save_credentials: True
save_credentials_backend: file
save_credentials_file: <STORAGE PATH, e.g. /usr/local/scripts/pydrive/credentials.json>

get_refresh_token: True

oauth_scope:
  - https://www.googleapis.com/auth/drive.file
'''

# Import general libraries
from os import chdir, listdir, stat
from sys import exit
import time
import ast

# Import Google libraries
from pydrive2.auth import GoogleAuth
from pydrive2.drive import GoogleDrive
from pydrive2.drive import GoogleDriveFileList
from pydrive2.settings import LoadSettingsFile
import googleapiclient.errors

# Update this value to the correct location.
#   e.g. "/usr/local/scripts/pydrive/settings.yaml"
PATH_TO_SETTINGS_FILE = None
assert PATH_TO_SETTINGS_FILE is not None  # Fail if path not specified.


# Update this value to the correct location.
#   e.g. "/home/user/gdrive/"
src_folder_name = None
assert src_folder_name is not None

# Update this value to the correct folder to create.
dst_folder_name = None
assert dst_folder_name is not None

# Update this value to the correct folder that exists gdrive.
parent_folder_name = None
assert parent_folder_name is not None

def authenticate():
    gauth = GoogleAuth()
    gauth.LocalWebserverAuth()
    return GoogleDrive(gauth)


def create_folder(drive, parent_folder_id, folder_name):

    # Check if destination folder exists and return it's ID
    file_list = GoogleDriveFileList()
    try:
        file_list = drive.ListFile(
                    {'q': "'{0}' in parents and trashed=false".format(parent_folder_id)}
		).GetList()

    # Exit if the parent folder doesn't exist
    except googleapiclient.errors.HttpError as err:
        message = ast.literal_eval(err.content)['error']['message']
        if message == 'File not found: ':
            print(message + folder_name)
            exit(1)
        else:
            raise

    for file1 in file_list:
        if file1['title'] == folder_name:
            print('title: %s, id: %s' % (file1['title'], file1['id']))
            return file1['id']


    # Create folder on Google Drive
    folder_metadata = {
        'title': folder_name,
        # Define the file type as folder
        'mimeType': 'application/vnd.google-apps.folder',
		# ID of the parent folder
		'parents': [{"kind": "drive#fileLink", "id": parent_folder_id}]
    }

    folder = drive.CreateFile(folder_metadata)
    folder.Upload()

    # Return folder informations
    folder_title = folder['title']
    folder_id = folder['id']
    print('title: %s, id, %s' % (folder_title, folder_id))


def upload_files(drive, folder_id, src_folder_name):
    try:
        chdir(src_folder_name)
    # Print error if source folder doesn't exist
    except OSError:
        print(src_folder_name + 'is missing')

    # Auto-iterate through all files in the folder.
    for files in listdir('.'):
        # Check the file's size
        statinfo = stat(files)
        if statinfo.st_size > 0:
            print('uploading ' + files)
            # Upload file to folder.
            f = drive.CreateFile(
                {"parents": [{"kind": "drive#fileLink", "id": folder_id}]})
            f.SetContentFile(files)
            f.Upload()
        # Skip the file if it's empty
        else:
            print('file {0} is empty'.format(files))


def main():

    # Authenticate to Google API
    drive = authenticate()

    # Get parent folder ID
    parent_folder_id = create_folder(drive, 'root', parent_folder_name)
    folder_id = create_folder(drive, parent_folder_id, dst_folder_name)

    # Create the folder if it doesn't exists
    if not folder_id:
        print('creating folder ' + dst_folder_name)
        folder_id = create_folder(drive, dst_folder_name, parent_folder_id)
    else:
        print('folder {0} already exists'.format(dst_folder_name))

        # Upload the files
        upload_files(drive, folder_id, src_folder_name)


if __name__ == '__main__':
    main()
